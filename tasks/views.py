from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import task_form
from projects.views import list_projects
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = task_form(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()
            return redirect(list_projects)
    else:
        form = task_form()
    return render(request, "tasks/create_task.html", {"form": form})


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    return render(request, "tasks/my_tasks.html", {"tasks": tasks})
