from django import forms
from projects.models import Project


class project_form(forms.ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]
